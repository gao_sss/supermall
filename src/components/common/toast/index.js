import Toast from './Toast'

const plugin = {}

function removeDom(event) {
	if (event.target.parentNode) {
		event.target.parentNode.removeChild(event.target)
	}
}

plugin.install = (Vue) => {
  //1.创建组件构造器
	const ToastConstructor = Vue.extend(Toast)

	// 注意:这里不能用箭头函数
	ToastConstructor.prototype.close = function() {
		this.isShow = false
		this.$el.addEventListener('transitionend', removeDom)
	}

	Vue.prototype.$toast = (options={}) => {
		// 2.根据组件构造器创建出组件对象
		const toast = new ToastConstructor()
    //3.将对象挂载到某个div中
		toast.$mount(document.createElement('div'))

		// 4.将toast的$el添加到body中
		document.body.appendChild(toast.$el)

		// 5.获取用户自定义数据
		toast.message = options.message
		toast.isShow = true
    
		//6.定时器让toast消失
    const duration = options.duration || 2500
		setTimeout(() => {
			toast.close()
		}, duration)
	}
}

export default plugin
