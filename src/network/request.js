import axios from 'axios'
import Qs from "qs";
export function request(config) {
  // 1.创建axios的实例
  const instance = axios.create({
    baseURL: 'http://123.207.32.32:8000',
    timeout: 5000
  })

  // 2.axios的拦截器
  // 2.1.请求拦截的作用
  instance.interceptors.request.use(config => {
    return config
  }, err => {
    // console.log(err);
  })

  // 2.2.响应拦截
  instance.interceptors.response.use(res => {
    return res.data
  }, err => {
    console.log(err);
  })

  // 3.发送真正的网络请求
  return instance(config)
}

export function request1(config) {
  // 1.创建axios的实例
  const instance1 = axios.create({
    baseURL: '/mobile',
    timeout: 5000,
    withCredentials: true,//是否支持跨域
    headers: {
      "Content-Type": "application/x-www-form-urlencoded;",
    },
    transformRequest: [
      function (data) {
        const newData={
          ...data,
          //apptoken:localStorage.getItem('apptoken') || 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMjA5MiIsInR5cGUiOiJ3eGFwcF90aW1lIiwiZXhwIjoxNjQzODcxNjA2fQ.xfgxiWmhv9RkNLfJ6YiAmxn5VhICphajp-zGyGb8WFg'
          apptoken:localStorage.getItem('apptoken') || 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjQzNjA5IiwidHlwZSI6Ind4YXBwX3RpbWUiLCJleHAiOjE2NDQ1NzI1Nzl9.ZK3TMrEsDBLzHkIrJtlCT22zN2ZFVHykFzu3cVN-47c'

        };
        data = Qs.stringify(newData);
        return data;
      },
    ],
  })

  // 2.axios的拦截器
  // 2.1.请求拦截的作用
  instance1.interceptors.request.use(config => {
    return config
  }, err => {
    // console.log(err);
  })

  // 2.2.响应拦截
  instance1.interceptors.response.use(res => {
    return res.data
  }, err => {
    console.log(err);
  })

  // 3.发送真正的网络请求
  return instance1(config)
}
