//mutations 专门做state的状态管理
const mutations = {
	add_to_cart(state, goods) {
		state.cartList.push(goods)
	},
	increment_count(state, index) {
		state.cartList[index].count += 1
	}
}


export default mutations
