module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        'assets': '@/assets',
        'common': '@/common',
        'components': '@/components',
        'network': '@/network',
        'views': '@/views',
      }
    }
  },devServer: {
    open: true, //自动打开浏览器
    compress: true, //热更新
    proxy: {
      "^/mobile": {
        target: "http://miniapp.asbeauty.cn",
        changeOrigin: true,
        rewrite:(path)=>path.replace('/^\/mobile/','/mobile/'),
        // pathRewrite: {
        //   "^/mobile": "/mobile",
        // },
      }
    }
  },
}

