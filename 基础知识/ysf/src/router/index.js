import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/homePage",
    name: "homePage",
    component: () => import("../views/homePage.vue"),
  },
  {
    path: "/Area",
    name: "Area",
    component: () => import("../views/Area.vue"),
  },
  {
    path: "/Team",
    name: "Team",
    component: () => import("../views/Team.vue"),
  },
  {
    path: "/areaList",
    name: "areaList",
    component: () => import("../views/areaList.vue"),
  },
  {
    path: "/AllTeam",
    name: "AllTeam",
    component: () => import("../views/AllTeam.vue"),
  },
  {
    path: "/fenTeam",
    name: "fenTeam",
    component: () => import("../views/fenTeam.vue"),
  },
  {
    path: "/childTeam",
    name: "childTeam",
    component: () => import("../views/childTeam.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
