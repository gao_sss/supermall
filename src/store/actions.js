const actions = {
	addToCart({state, commit}, info) {
		return new Promise((resolve) => {
			// 1.查看该商品是否已经添加到cartList中
			const oldInfo = state.cartList.find(item => item.iid === info.iid)

			// 2.如果oldInfo存在, 那么原来的数量加1
			if (oldInfo) {
				const index = state.cartList.indexOf(oldInfo)
				commit('increment_count', index)
			} else {
				info.count = 1
				info.checked = true
				commit('add_to_cart', info)
			}
			resolve('加入购物车成功') //这样外部调用就可以链式使用then
		})
	}
}

export default actions
