// export function debounce(func, delay) {
//   let timer = null
//   return function (...args) {
//     if (timer) clearTimeout(timer)
//     timer = setTimeout(() => {
//       func.apply(this, args)
//     }, delay)
//   }
// }
// window.onscroll在每次滚动的时候，都会被调用debounce(updateCount, 1000)，
// 在debounce函数内部，如果之前已经存在定时器，那么就清除已有的定时器，重新开始计时。
// 这样，如果滚动过于频繁地被触发，则之前滚动所开启的定时器都会被紧接而来的下一个滚动事件清除，
// 只有最后一个滚动事件触发的定时器才会被保存，最终在1000ms之后执行updateCount函数。
export function debounce(fn, delay, isImmediate=null) {
  var timer = null;  //初始化timer，作为计时清除依据
  return function() {
    var args = arguments;  //取得传入参数
    if (timer) clearTimeout(timer);
    if(isImmediate && timer === null) {
      //时间间隔外立即执行
      fn.apply(this,args);
      timer = 0;
      return;
    }
    timer = setTimeout(function() {
      fn.apply(this,args);
      timer = null;
    }, delay);
  }
}

export function throttle(fn, delay) {
  var timer = null;
  var timeStamp = new Date();
  return function() {
    var context = this;  //获取函数所在作用域this
    var args = arguments;  //取得传入参数
    if(new Date()-timeStamp>delay){
      timeStamp = new Date();
      timer = setTimeout(function(){
        fn.apply(context,args);
      },delay);
    }

  }
}

export function formatDate(date, fmt) {
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
  }
  let o = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds()
  };
  for (let k in o) {
    if (new RegExp(`(${k})`).test(fmt)) {
      let str = o[k] + '';
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str));
    }
  }
  return fmt;
};

function padLeftZero (str) {
  return ('00' + str).substr(str.length);
};



