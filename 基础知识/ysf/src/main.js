import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import { DropdownMenu, Button } from "element-ui";
import Qs from "qs";
import Vant from 'vant';
import 'vant/lib/index.css';

import axios from "axios"; //引入axios
const http = axios.create({
  headers: {
    "Content-Type": "application/x-www-form-urlencoded;",
  },
  transformRequest: [
    function (data) {
      data = Qs.stringify(data);
      return data;
    },
  ],
});
// 时间戳转为时间 yyyy-MM-dd hh:mm:ss

http.interceptors.response.use(respone => {
	console.log('请求结果', respone)
	return respone.data
	
}, err => {
	console.log(err)
})
axios.defaults.baseURL = "/api";
axios.defaults.withCredentials = true;
Vue.prototype.$axios = http; //挂载在原型上
Vue.use(Vant);
Vue.use(ElementUI);
Vue.config.productionTip = false;
Vue.use(DropdownMenu);
Vue.use(Button);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
