import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Toast from 'components/common/toast'
import Fastclick from 'fastclick'
import LazyLoad from 'vue-lazyload'
// import {request,request1} from "network/requestnpmnpm
// Vue.prototype.$request = request; //挂载在原型上
// Vue.prototype.$request1 = request1; //挂载在原型上
Vue.prototype.$bus = new Vue();
Vue.use(Toast)
//解决移动端延迟300ms的click事件
Fastclick.attach(document.body)
Vue.use(LazyLoad, {
  loading: require('assets/img/common/placeholder.png')
})
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
