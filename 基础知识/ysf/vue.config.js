const webpack = require("webpack");
module.exports = {
  //配置webpack选项的内容
  // publicPath: process.env.NODE_ENV === 'production' ? "/oa/dist/" : "/", //打包路径
  publicPath: process.env.NODE_ENV === 'production' ? "./" : "/", //打包路径
  devServer: {
    open: true, //自动打开浏览器
    compress: true, //热更新
    proxy: {
      "/mobile": {
        target: "https://mdttest.asbeauty.cn",
        changeOrigin: true,
        pathRewrite: {
          "^/mobile": "/mobile",
        },
      },
    },
  },
};


