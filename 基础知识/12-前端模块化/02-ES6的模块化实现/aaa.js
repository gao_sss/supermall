var name = '小明'
var age = 18
var flag = true

function sum(num1, num2) {
  return num1 + num2
}

if (flag) {
  console.log(sum(20, 30));
}

// 1.导出方式一:
export {
  flag, sum
}

// 2.导出方式二:
export var num1 = 1000;
export var height = 1.88


// 3.导出函数/类
export function mul(num1, num2) {
  return num1 * num2
}

export class Person {
  run() {
    console.log('在奔跑');
  }
}

//export default 1.一个文件只能有一个，作用是导入时候可以更改导出的名字，比如导出的是address，导入时候我可以自定义名称比如addr。这样addr就代表address
//export default 2.如果导出的是函数，甚至可以省略函数名，因为在导入时候可以自定义名称
// 5.export default
// const address = '北京市'
// export {
//   address
// }
// export const address = '北京市'

// const address = '北京市'
// export default address

export default function (argument) {
  console.log(argument);
}







