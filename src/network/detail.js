import {request,request1} from "network/request"

export function getDetail(goods_id) {
  return request1({
    method: 'post',
    url: '/index.php?m=goods&c=nyzshop&a=goods_Detail',
    data: {
      goods_id,
    }
  })
}

// export class Goods { --老师数据比较复杂，而我的比较简单
//   constructor(itemInfo, columns, services) {
//     this.title = itemInfo.title
//     this.desc = itemInfo.desc
//     this.newPrice = itemInfo.price
//     this.oldPrice = itemInfo.oldPrice
//     this.discount = itemInfo.discountDesc
//     this.columns = columns
//     this.services = services
//     this.realPrice = itemInfo.lowNowPrice
//   }
// }

export class Goods {
  constructor(itemInfo) {
    this.title = itemInfo.goods_name
    this.desc = itemInfo.goods_alias_name
    this.newPrice = itemInfo.user_discount_price
    this.oldPrice = itemInfo.shop_price
    this.discount = itemInfo.user_discount
    this.columns = ['不让报错1','不让报错2','不让报错3']
    this.services = [{'name':'测试名字1','icon':''},{'name':'测试名字2','icon':''},{'name':'测试名字3','icon':''},{'name':'测试名字4','icon':''}]
    this.realPrice = itemInfo.user_discount_price
  }
}


export class Shop {
  constructor(shopInfo) {
    this.logo = shopInfo.brand_thumb;
    this.name = shopInfo.shop_name;
    this.fans = shopInfo.fictitious_fans_num;
    this.sells = 10000;
    this.score = [{'isBetter':true,name:'描述相符','score':5},{'isBetter':true,name:'描述相符','score':5},{'isBetter':true,name:'描述相符','score':5}];
    this.goodsCount = 100
  }
}


export class GoodsParam {
  constructor(info, rule) {
    // 注: images可能没有值(某些商品有值, 某些没有值)
    this.image = info.images ? info.images[0] : '';
    this.infos = info.set;
    this.sizes = rule.tables;
  }
}





